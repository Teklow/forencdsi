Metadata-Version: 2.1
Name: regrippy
Version: 1.0.1
Summary: A modern Python-3-based alternative to RegRipper
Home-page: https://github.com/airbus-cert/regrippy
Author: Airbus CERT
Author-email: cert@airbus.com
License: UNKNOWN
Description: RegRip**py** is a framework for reading and extracting useful forensics data from Windows registry hives. It is an alternative to [RegRipper](https://github.com/keydet89/RegRipper2.8) developed in modern Python 3. It makes use of William Ballenthin's [python-registry](https://github.com/williballenthin/python-registry) to access the raw registry hives.
        
        The goal of this project is to provide a framework for quickly and easily developing your own plugins in an incident response scenario.
        
        By default, the script will look for the various hives by reading the `REG_SYSTEM`, `REG_SOFTWARE`, `REG_SAM`, `REG_NTUSER` and `REG_USRCLASS` environment variables. This allows the analyst to simply `export` these in their current shell session and not have to worry about specifying them every time they invoke the script.
        Alternatively, you can use the `--root` switch to specify the path to the root of the `C:` drive. RegRippy will automatically look into the right places depending on which hive each plugin needs.
        
        All plugins should also support both a human-readable and machine-readable output (the [Bodyfile](https://wiki.sleuthkit.org/index.php?title=Body_file) format), allowing easy piping to `mactime` or other tools.
            
Platform: UNKNOWN
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3 :: Only
Classifier: Environment :: Console
Classifier: Operating System :: OS Independent
Classifier: License :: OSI Approved :: Apache Software License
Classifier: Topic :: Utilities
Classifier: Intended Audience :: Information Technology
Requires-Python: >=3.6
Description-Content-Type: text/markdown

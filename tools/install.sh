#!/bin/bash

# Install Python2.7
sudo apt-get install -y python

# Install Python2.7 Pip
sudo apt-get install -y curl
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
sudo python2.7 get-pip.py

# Install Python3 Pip
sudo apt-get remove -y python3-pip; sudo apt-get install -y python3-pip

# Build and install fresh yara-python (Known error when not build specifically)
sudo pip install --global-option="build" --global-option="enable-cuckoo" --global-option="--enable-magic" yara-python
pip install --global-option="build" --global-option="enable-cuckoo" --global-option="--enable-magic" yara-python
sudo pip3 install --global-option="build" --global-option="enable-cuckoo" --global-option="--enable-magic" yara-python
pip3 install --global-option="build" --global-option="enable-cuckoo" --global-option="--enable-magic" yara-python

# Install Malscan
sudo bash malscan-1.x/installer.sh

# Install NTFS_Parser requirements
cd dfir_ntfs/
sudo python3 setup.py install
cd ..

# Install event2timeline requirements (Python 2.7)
sudo pip2 install -r event2timeline/requirements.txt
pip2 install -r event2timeline/requirements.txt

# Install Regrippy
cd regrippy/
sudo python3 setup.py install
pip3 install -r requirements.txt
sudo pip3 install -r requirements.txt
cd ..

# Install Loki and upgrading SIX module
cd Loki/
sudo pip3 install -r requirements.txt
pip3 install -r requirements.txt
sudo pip install -r requirements.txt
pip install -r requirements.txt
cd ..


# Install Sleuthkit
sudo apt-get install -y sleuthkit
 
# Install Recursive-virustotal
pip3 install -r recursive-virustotal/requirements.txt
sudo pip3 install -r recursive-virustotal/requirements.txt

# Install fdfind
sudo apt-get install -y fd-find

# Install libevtx-utils
sudo apt-get install -y libevtx-utils

# Install Log2Timeline
sudo apt-get install -y plaso

# Install easygui
python3 -m pip install easygui

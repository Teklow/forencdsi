#!/bin/bash
while getopts ":v:V:n:N:hHp:" flag
do
    case "${flag}" in
        v) vdata=${OPTARG};;
        n) nvdata=${OPTARG};;
	V) vdata=${OPTARG};;
	h) ;;
	N) nvdata=${OPTARG};;
	H) ;;
	p) profil=${OPTARG};;
	:) echo "Erreur : argument requis pour -$OPTARG" 1>&2 && exit;;
	\?) echo " Besoin d'aide ?" && (firefox&) && exit;;  # faire une page d aide a renseigner en param de firefox
    esac
done
# Test si aucun flag n'est passé en parametre "bash dat.sh" (getopt ne se lance alors pas)
if [ "$OPTIND" -eq 1 ]
then 
	shift $((OPTIND -1)) # Convention pour clear les options déjà passées
	echo " Besoin d'aide ?" && (firefox&) && exit # faire une page d aide a renseigner en param de firefox 
elif [[ -z "$vdata" && -z "$nvdata"  ]] || [ $# -lt 1 ]
then 
	echo  " Aucun fichier à analyser.... " &&  exit;
elif [[ -n "$vdata" && -e "$vdata" && -f "$vdata"  && -r "$vdata" ]];  
then
	verif=$(file $vdata 2>&1)
	if [[ "$verif"  == *"data"* ]];
	then
		echo "Check hash of dump for integrity ....."

		hashmd5=`md5sum "$vdata"`
		hashsha1=`sha1sum "$vdata"`

		echo "######################################"
		echo "#####           HASHES :         #####"
		echo "######################################"
		echo "SHA-1 : "$hashsha1""
		echo "MD5 : "$hashmd5""
		
		read -p "Intégrité du dump correct ? Y/N " res 
		if [[ $res == "Y" || $res == "y" ]]
		then
			echo "Analyse du dump de mémoire volatile (mémoire vive)...";
			#do something with volatility
		else
			echo "Stopping the analysis." && exit;
		fi
	else
		echo "Ce fichier n'est pas un dump mémoire" && exit;
	fi
elif [[ -n "$nvdata" &&  -e "$nvdata"  &&  -f "$nvdata"  &&  -r "$nvdata" ]];  
then
	verif=$(file "$nvdata" 2>&1)
	if [[ "$verif"  == *"DOS"* ]];
	then 
		echo "Check hash of dump for integrity ....."

		hashmd5=`md5sum "$nvdata"`
		hashsha1=`sha1sum "$nvdata"`

		echo "######################################"
		echo "#####           HASHES :         #####"
		echo "######################################"
		echo "SHA-1 : "$hashsha1""
		echo "MD5 : "$hashmd5""
		
		read -p "Intégrité du dump correct ? Y/N " res 
		if [[ $res == "Y" || $res == "y" ]]
		then
			echo "Analyse du dump de mémoire non-volatile (mémoire FLASH)...";
			#do something with volatility
		else
			echo "Stopping the analysis." && exit;
		fi
	else
		echo "Ce fichier n'est pas un dump mémoire cas 2" && exit;
	fi
fi
 

 
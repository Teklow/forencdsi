# Projet ForenCDSI 

Ce projet a pour but de fournir un Toolkit de premières analyses sur des images de mémoire non-volatile et volatile afin de pouvoir rapidement avoir un visuel sur la situation à la suite d'une compromission de données survenue au sein d'un système d'information.

Ce projet de fin d'étude a été réalisé dans le cadre du **Master Cyber-Défense et Sécurité de l'Information** au sein de l'INSA Hauts-de-France.

# Auteurs

Ce projet a été réalisé par :
 - **KOUKOUI Lorens**
 - **DJELMOUDI Younès**


## Dépot 

Vous retrouverez l'outil ainsi que le dépot Gitlab complet et ouvert à tous [ici](https://gitlab.com/Teklow/forencdsi).
  
Pour tout contact : @Teklow sur Gitlab
###### *UPHF - INSA HAUTS-DE-FRANCE / 2020-2022*

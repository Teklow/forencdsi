#!/bin/bash

dia=$(date '+%Y%m%d%H%M');
echo 'Detection type de dump :';
COLUMNS=30; # taille des colonnes (nombres de caracteres) du terminal pour mieux afficher le menu d'analyse
# Lancement de la détection
profile=`./profile_detect.sh "$1"`
echo "Profil détecté : ${profile}";

# Menu d'analyse de la RAM
# A faire , les lancements des commandes pour chaque analyse (script a part ?)
options=("Analyse des processus" "Analyse DLL" "Analyse Injection de code" "Analyse des connections Réseau" "Analyse du Registre" "Générer une timeline" "Quitter")
select opt in "${options[@]}"
do
    case "$REPLY" in
        1)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";
            suboptions=("pstree" "pslist" "psscan" "psxview" "quit");
            select opt in "${suboptions[@]}"
            do
                case "$REPLY" in
                    1)  
                        echo "Listages des processus en arborescence : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    2)  
                        echo "Listages des processus : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    3)  
                        echo "Listages des processus par : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    4)  
                        echo "Listages des processus par : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    5)  
                        break
                        ;;
                    *)
                        echo "Choix inconnu";;
                esac
                REPLY= ;
            done
            ;;
        2)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";

            # à améliorer pour prendre en compte un PID
            command="-f ${1} --profile=${profile} dlllist";
            output=`./volatility_execution.sh "$command"`;
            echo "${output}" >> "malfind"."$dia".af;
            ;;
        3)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";

            command="-f ${1} --profile=${profile} malfind";
            output=`./volatility_execution.sh "$command"`;
            echo "${output}" >> "malfind"."$dia".af;
            ;;
        4)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";
            suboptions=("netscan" "sockscan" "connscan" "connections" "socket" "Quit");
            select opt in "${suboptions[@]}"
            do
                case "$REPLY" in
                    1)  
                        echo "Listages des connections : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    2)  # Commande reservé aux dump WinXP/Win2003
                        if [[ "$profile" == *"WinXP"* || "$profile" == *"Win2003"* ]]
                        then
                            echo "Listages des connections : $opt";
                            command="-f ${1} --profile=${profile} ${opt}";
                            output=`./volatility_execution.sh "$command"`;
                            echo "${output}" >> "$opt"."$dia".af;
                        else
                            echo "$opt disponible uniquement pour les dump Window XP/Server 2003";
                        fi
                        ;;
                    3)  # Commande reservé aux dump WinXP/Win2003
                        if [[ "$profile" == *"WinXP"* || "$profile" == *"Win2003"* ]]
                        then
                            echo "Listages des connections : $opt";
                            command="-f ${1} --profile=${profile} ${opt}";
                            output=`./volatility_execution.sh "$command"`;
                            echo "${output}" >> "$opt"."$dia".af;
                        else
                            echo "$opt disponible uniquement pour les dump Window XP/Server 2003";
                        fi
                        ;;
                    5)  # Commande reservé aux dump WinXP/Win2003
                        if [[ "$profile" == *"WinXP"* || "$profile" == *"Win2003"* ]]
                        then
                            echo "Listages des connections : $opt";
                            command="-f ${1} --profile=${profile} ${opt}";
                            output=`./volatility_execution.sh "$command"`;
                            echo "${output}" >> "$opt"."$dia".af;
                        else
                            echo "$opt disponible uniquement pour les dump Window XP/Server 2003";
                        fi
                        ;;   
                    5)  # Commande reservé aux dump WinXP/Win2003
                        if [[ "$profile" == *"WinXP"* || "$profile" == *"Win2003"* ]]
                        then
                            echo "Listages des connections : $opt";
                            command="-f ${1} --profile=${profile} ${opt}";
                            output=`./volatility_execution.sh "$command"`;
                            echo "${output}" >> "$opt"."$dia".af;
                        else
                            echo "$opt disponible uniquement pour les dump Window XP/Server 2003";
                        fi
                        ;;
                    6)  
                        break
                        ;;
                    *)
                        echo "Choix inconnu";;
                esac
                REPLY= ;
            done
            ;;
        5)
            echo "";
            echo "######### "$opt" ##########";
            echo "";
            suboptions=("hivescan" "hivelist" "hivedump" "Quit");
            select opt in "${suboptions[@]}"
            do
                case "$REPLY" in
                    1)  
                        echo "Listages des adresses mémoires des ruches de registre : $opt";
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    2)  
                        echo "Listages des ruches de registre : $opt"
                        command="-f ${1} --profile=${profile} ${opt}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    3)  
                        echo "Listages des clés de la ruche spécifique: $opt"
                        read -p "Quelle est l'adresse de la ruche à examiner ? : " $adresse_ruche
                        command="-f ${1} --profile=${profile} ${opt} -o ${adresse_ruche}";
                        output=`./volatility_execution.sh "$command"`;
                        echo "${output}" >> "$opt"."$dia".af;
                        ;;
                    4)  
                        break
                        ;;
                    *)
                        echo "Choix inconnu";;
                esac
                REPLY= ;
            done
            ;;
        6)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";

            command="-f ${1} --profile=${profile} timeliner";
            output=`./volatility_execution.sh "$command"`;
            echo "${output}" >> "timeliner"."$dia".af;
            ;;
        
        7)
            echo "";
            echo "######### Arret de l'analyse ##########";
            echo "";
            break;
            ;;
        

        *) echo "Choix inconnu"; 
            ;;
    esac
    # Reset de la variable REPLY pour réafficher le menu apres un choix (a voir si indispensable)
    REPLY= ;

done
#!/bin/bash


if [[ $# -ne 1 ]] || [[ -d $1 ]]
then
    echo "Erreur : chemin du dump requis ou non correct" && exit;
else
    CHEMIN=$1
    output=`./../tools/volatility_2.6 kdbgscan -f "$CHEMIN" | awk -F ":" '$1 == "Profile suggestion (KDBGHeader)"{print substr($2,2);}' | head -1` ;
    echo "${output}";
fi

exit 0
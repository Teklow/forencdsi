#! /bin/bash

python2.7 ./../tools/event2timeline/event2timeline.py -e -f $1
exitcode=$?;
if [ "${exitcode}" -ne 1 ];
then
	mkdir -p ./../results/EventLogs/$2.$3
	cp ./../tools/event2timeline/timeline/evtdata.js ./../results/EventLogs/$2.$3/evtdata.js
	cp ./../tools/event2timeline/timeline/d3.v2.js ./../results/EventLogs/$2.$3/d3.v2.js
	cp ./../tools/event2timeline/timeline/timeline-sessions.html ./../results/EventLogs/$2.$3/timeline.$2.$3.html
fi
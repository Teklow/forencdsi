#!/bin/bash


if [[ $# -ne 1 ]] || [[ -d $1 ]]
then
    echo "Erreur : chemin du dump requis ou non correct" && exit;
else
    CHEMIN=$1
    
    MMLS=`mmls "$CHEMIN"`
    
    if [ -z "$MMLS" ];
    then
        OUTPUT=`fsstat "$CHEMIN"` ;
        OFFSET=0;

        # Test du type de partition
        FS=`echo "$OUTPUT" | grep -Po "(?<=File System Type: ).*"`; 
        if [[ "$FS" == "NTFS" ]];
        then
            OUTPUT="Windows";
        elif [[ "$FS" == "HFS+" ]] || [[ "$FS" == "HFS" ]]
        then
            OUTPUT="Mac";
        else
            OUTPUT="Unknown";
        fi
    else

        OFFSET=`echo "$MMLS" | grep -P "(NTFS|HFS|HFS+)" | cut -f6 -d' '` ;
        OFFSET=`expr $OFFSET + 0`
        OUTPUT=`fsstat -o "${OFFSET}" "$CHEMIN"` ;
        # Test du type de partition
        FS=`echo "$OUTPUT" | grep -Po "(?<=File System Type: ).*"`; 

        if [[ "$FS" == "NTFS" ]]
        then
            OUTPUT="Windows";
        elif [[ "$FS" == "HFS+" ]] || [[ "$FS" == "HFS" ]]
        then
            OUTPUT="Mac";
        else
            OUTPUT="Unknown";
        fi
    fi
    echo "${OUTPUT} Offset ${OFFSET}";
fi

exit 0
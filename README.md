# ForenCDSI

[![made-with-bash](https://img.shields.io/badge/Made%20with-Bash-1f425f.svg)](https://www.gnu.org/software/bash/)  
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)  

ForenCDSI eases Forensic analysis by analyzing RAM and HDD/USB dumps automatically with useful artefacts

### RAM Analysis
The RAM analysis includes different type of analysis based on useful artifacts :

- DLL : [DllList](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#dlllist)
- Processes : [PsTree](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/pstree.py), [PsList](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#pslist), [PsScan](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#psscan), [PsxView](https://github.com/volatilityfoundation/volatility/wiki/#psxview)
- Code Injections : [Malfind](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/malware/malfind.py)
- Networks : [NetScan](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/netscan.py), [SockScan](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/sockscan.py), [ConnScan](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/connscan.py), [Connections](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/connections.py), [Socket](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/sockets.py)
- Registers : [HiveScan](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/registry/hivescan.py), [HiveList](https://github.com/volatilityfoundation/volatility/blob/master/volatility/plugins/registry/hivelist.py), [HiveDump](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#hivedump)
- Timeline Generations : [Timeliner](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#timeliner)

All of those analysis are powered by [Volatility Framework](https://github.com/volatilityfoundation/volatility)



### HDD Analysis
The HDD analysis includes different type of analysis based on useful artifacts :

- Automatic OS Detection and Mounting :
- Viral Analysis : [Malscan](https://github.com/malscan/malscan), [Loki IOC Scanner](https://github.com/Neo23x0/Loki), [VirusTotalAPI - Recursive](https://github.com/FaVorith/recursive-virustotal)  
- Registers : [Regrippy](https://github.com/airbus-cert/regrippy) (Uses of various plugins for different usecases)
- Windows Event Logs : [EVTXExport](https://manpages.ubuntu.com/manpages/xenial/man1/evtxexport.1.html#author), [Event2Timeline](https://github.com/certsocietegenerale/event2timeline)
- Timelines (MFT and Partitions) : [The Sleuth Kit](https://github.com/sleuthkit/sleuthkit), [NTFS Parser](https://github.com/msuhanov/dfir_ntfs) and [Log2Timeline](https://github.com/log2timeline/plaso)

### Results
All of the results will be in the Result/* folders in different formats :

- _Text Format_
- _CSV Format_
- _HTML Format_
- _PDF Format_

## License

This project is under the UPHF/INSA Licence.


#!/bin/bash


if [[ $# -ne 1 ]]
then
    echo "Erreur commande" && exit;
else
    COMMAND=$1
    output=`python3 ./../tools/regrippy/regrip.py --root /mnt/temp/ --all-user-hives ${COMMAND}`;
    echo "${output}";
fi

exit 0
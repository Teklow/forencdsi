#!/bin/bash


if [[ $# -ne 1 ]]
then
    echo "Erreur commande" && exit;
else
    COMMAND=$1
    output=`./../tools/volatility_2.6 ${COMMAND}` ;
    echo "${output}";
fi

exit 0
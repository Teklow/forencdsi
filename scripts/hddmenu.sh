#!/bin/bash

dia=$(date '+%Y%m%d%H%M');
echo 'Detection type de dump :';
COLUMNS=0; # taille des colonnes (nombres de caracteres) du terminal pour mieux afficher le menu d'analyse
# Lancement de la détection
profile=`./os_detect.sh "$1"`
offset=0

# Si on fait face à un offset de la partition spécifique alors on le récupere puis on le sauvegarde pour les futures commandes
if [[ $profile == *"Offset"* ]];
then
    offset=`echo "$profile" | cut -d' ' -f3` ;
    profile=`echo "$profile" | cut -d' ' -f1` ;
fi
echo "Profil détecté : ${profile}";
echo "";

echo "";
echo "######### Image Mounting ##########";
echo "";
            
bytesSector=`expr $offset \* 512`
dir=`sudo mkdir -p /mnt/temp`
extract=`sudo mount -o ro,offset=${bytesSector},loop,show_sys_files $1 /mnt/temp`;
path='/mnt/temp/';
echo "Mounted in $path";
echo "";


# Menu d'analyse de la mémoire non-volatile (HDD,USB,etc.)

options=("Analyses Virales" "Analyses du Registre" "Analyse des Journaux" "Génération(s) timeline(s)" "Quitter")
select opt in "${options[@]}"
do
    case "$REPLY" in

        1)
            if grep -qs '/mnt/temp ' /proc/mounts; 
            then
                echo "";
                echo "######### "${opt}" ##########";
                echo "";
            

                suboptions=("Scan Malscan (ClamAV)" "Scan Loki (via Yara)" "Scan VirusTotal" "Launch every scan" "Quit");
                select opt in "${suboptions[@]}"
                do
                    echo "";
                    echo "######### "${opt}" ##########";
                    echo "";
                    case "$REPLY" in
                        1)  
                            echo -en "Viral scanning through Malscan (\e[96;4mpending...\e[39;24m)"
                            # On retire les caracteres de couleurs du retour de malscan pour avoir un output propre
                            output=`sudo malscan -alm /mnt/temp | sed 's/\x1b\[[0-9;]*m//g'`;
                            echo -e "\r\e[KViral scanning through Malscan (\e[92;4mDONE!\e[39;24m)";
                            echo "${output}" >> "./../results/viral/malscan"."$dia".af;
                            echo "";
                            echo "Results in : results/viral/malscan."$dia".af";
                            echo "";
                            ;;

                        2)  
                            echo -en "Viral scanning through Loki (\e[96;4mpending...\e[39;24m)"
    
                            output=`sudo python3 ./../tools/Loki/loki.py -p /mnt/temp/ --noprocscan --nolisten --dontwait --onlyrelevant --intense -l ./../results/viral/lokiscan."$dia".af`;
                            echo -e "\r\e[KViral scanning through Loki (\e[92;4mDONE!\e[39;24m)"
                            echo "";
                            echo "Results in : results/viral/lokiscan."$dia".af";
                            echo "";
                            ;;


                        3)    # A désactiver pour le moment cause : Api gratuite donc restreinte
                            echo "Disabled for the moment.";
                            break;
                            echo -en "Viral scanning through VirusTotal (\e[96;4mpending...\e[39;24m)"
                            output=`python3 ./../tools/recursive-virustotal/recursive-vt.py -p /mnt/temp/ > ./../results/viral/virustotal."$dia".af`;
                            echo -e "\r\e[KViral scanning through VirusTotal (\e[92;4mDONE!\e[39;24m)"
                            echo "${output}" >> "./../results/viral/virustotal"."$dia".af;
                            ;;

                        4)  # Malscan
                            echo -en "Viral scanning through Malscan (\e[96;4mpending...\e[39;24m)"
                            # On retire les caracteres de couleurs du retour de malscan pour avoir un output propre
                            output=`sudo malscan -alm /mnt/temp | sed 's/\x1b\[[0-9;]*m//g'`;
                            echo -e "\r\e[KViral scanning through Malscan (\e[92;4mDONE!\e[39;24m) -> Results in : results/viral/malscan."$dia".af"
                            echo "${output}" >> "./../results/viral/malscan"."$dia".af;
                            
                            # Loki

                            echo -en "Viral scanning through Loki (\e[96;4mpending...\e[39;24m)"
    
                            output=`sudo python3 ./../tools/Loki/loki.py -p /mnt/temp/ --noprocscan --nolisten --dontwait --onlyrelevant --intense -l ./../results/viral/lokiscan."$dia".af`;
                            echo -e "\r\e[KViral scanning through Loki (\e[92;4mDONE!\e[39;24m) -> Results in : results/viral/lokiscan."$dia".af"
                           

                            # Virustotal
                            # A désactiver pour le moment cause : Api gratuite donc restreinte
                            echo "Disabled for the moment.";
                            break;
                            echo -en "Viral scanning through VirusTotal (\e[96;4mpending...\e[39;24m)"
                            output=`python3 ./../tools/recursive-virustotal/recursive-vt.py -p /mnt/temp/ > ./../results/virustotal."$dia".af`;
                            echo -e "\r\e[KViral scanning through VirusTotal (\e[92;4mDONE!\e[39;24m) -> -> Results in : results/virustotal."$dia".af"
                            echo "${output}" >> "/../results/virustotal"."$dia".af;
                            echo "";
                            echo "";
                            ;;   
                        5)  
                            REPLY= ;
                            break;
                            ;;
                        *)
                            echo "Choix inconnu";;
                    esac
                    REPLY= ; 
                done
                REPLY= ;
            else
                echo "Viral analysis needs a mounted image to work on.\nPlease mount a dump image.";
                echo "";
            fi
            REPLY= ; 
            ;;

        ################### Encore en cours de dev ###############################
        2)
            echo "";
            echo "######### "${opt}" ##########";
            echo "";
            suboptions=("Analyse Système" "Analyse Utilisateurs/Groupes" "Analyse Réseau" "Analyse Programmes" "Analyse Fichiers" "Quit");
            select opt in "${suboptions[@]}"
            do
                echo "";
                echo "######### "${opt}" ##########";
                echo "";
                case "$REPLY" in

                    # Génération rapport Analyse Système
                    1)  # Systeminfo command
                        echo -en "System Analysis in Registry hives by Regrippy (\e[96;4mpending...\e[39;24m)";
                        
                        echo "" >> "./../results/registers/systeminfo."$dia".af";
                        echo "######### System General Informations ##########">> "./../results/registers/systeminfo."$dia".af";
                        echo "" >> "./../results/registers/systeminfo."$dia".af";
                        command="systeminfo";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/systeminfo"."$dia"."af";
                        
                        # Keyboard command
                        echo "" >> "./../results/registers/systeminfo."$dia".af";
                        echo "######### Keyboard Languages ##########">> "./../results/registers/systeminfo."$dia".af";
                        echo "" >> "./../results/registers/systeminfo."$dia".af";
                        command="keyboard";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/systeminfo"."$dia"."af";
                        
                        echo -e "\r\e[KSystem Analysis in Registry hives by Regrippy (\e[92;4mDONE!\e[39;24m)"
                        echo "";
                        echo "";
                        ;;


                    # Génération rapport Analyse Utilisateurs/Groupes
                    2)  
                        echo -en "Users/Groups Analysis in Registry hives by Regrippy (\e[96;4mpending...\e[39;24m)"

                        # Localusers command
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        echo "######### Local Users ##########">> "./../results/registers/usersinfos."$dia".af";
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        command="localusers";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/usersinfos"."$dia"."af";

                        # Usersids command
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        echo "######### Users IDs ##########">> "./../results/registers/usersinfos."$dia".af";
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        command="usersids";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/usersinfos"."$dia"."af";

                        # Lastloggedon command
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        echo "######### Last User Logged On ##########">> "./../results/registers/usersinfos."$dia".af";
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        command="lastloggedon";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/usersinfos"."$dia"."af";

                        # Localgroups command
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        echo "######### Local Groups ##########">> "./../results/registers/usersinfos."$dia".af";
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        command="localgroups";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/usersinfos"."$dia"."af";

                        # Gpo command
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        echo "######### Group Policy Objects ##########">> "./../results/registers/usersinfos."$dia".af";
                        echo "" >> "./../results/registers/usersinfos."$dia".af";
                        command="gpo";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/usersinfos"."$dia"."af";

                        echo -e "\r\e[KUsers/Groups Analysis in Registry hives by Regrippy (\e[92;4mDONE!\e[39;24m)"
                        echo "";
                        echo "";
                        ;;
                   

                    # Génération rapport Analyse Réseau
                   3)   
                        echo -en "Network Analysis in Registry hives by Regrippy (\e[96;4mpending...\e[39;24m)"
                        
                        # Portproxy command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### Redirected Network Connections ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="portproxy";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        # proxy command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### Users' Proxy Settings ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="proxy";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        # Printer_history command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### Printers History ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="printer_history";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        # Printer_ports command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### Printers Ports Setup ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="printer_ports";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        # Rdphint command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### RDP Connections History ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="rdphint";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        # Putty command
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        echo "######### PuTTY Connections History (SSH) ##########">> "./../results/registers/connectionsinfo."$dia".af";
                        echo "" >> "./../results/registers/connectionsinfo."$dia".af";
                        command="putty";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/connectionsinfo"."$dia"."af";

                        echo -e "\r\e[KNetwork Analysis in Registry hives by Regrippy (\e[92;4mDONE!\e[39;24m)"
                        echo "";
                        echo "";
                        ;;
                   
                    # Génération rapport Analyse Programmes
                    4)  
                        echo -en "Program Analysis in Registry hives by Regrippy (\e[96;4mpending...\e[39;24m)"

                        # teamviewer command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### TeamViewer Configuration ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="teamviewer";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # typedurls command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Users' Typed URLs ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="typedurls";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # uninstall command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Installed Programs ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="uninstall";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # services command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Installed Services ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="services";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # run command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Startup Programs ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="run";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # runmru command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Most Recently Used Programs ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="runmru";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        # userassist command
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        echo "######### Program Usage Information ##########">> "./../results/registers/programsinfo."$dia".af";
                        echo "" >> "./../results/registers/programsinfo."$dia".af";
                        command="userassist";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/programsinfo"."$dia"."af";

                        echo -e "\r\e[KProgram Analysis in Registry hives by Regrippy (\e[92;4mDONE!\e[39;24m)"
                        echo "";
                        echo "";
                        ;;   

                    # Génération rapport Analyse Fichiers
                    5)  
                        echo -en "Files Analysis in Registry hives by Regrippy (\e[96;4mpending...\e[39;24m)"
                        
                        # Recentdocs command
                        echo "" >> "./../results/registers/filesinfo."$dia".af";
                        echo "######### Users' Recent Documents ##########">> "./../results/registers/filesinfo."$dia".af";
                        echo "" >> "./../results/registers/filesinfo."$dia".af";
                        command="recentdocs";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/filesinfo"."$dia"."af";

                        # Filedialogmru command
                        echo "" >> "./../results/registers/filesinfo."$dia".af";
                        echo "######### Users' Recent Save/Open Dialog Documents ##########">> "./../results/registers/filesinfo."$dia".af";
                        echo "" >> "./../results/registers/filesinfo."$dia".af";
                        command="filedialogmru";
                        output=`./regrippy_execution.sh "$command"`;
                        echo "${output}" >> "./../results/registers/filesinfo"."$dia"."af";
                        
                        echo -e "\r\e[KFiles Analysis in Registry hives by Regrippy (\e[92;4mDONE!\e[39;24m)"
                        echo "";
                        echo "";
                        ;;
                    6)  # Quit
                        echo "";
                        echo "";
                        break
                        ;;
                    *)
                        echo "Choix inconnu";
                        echo "";
                        echo "";
                        ;;
                esac
                REPLY= ;
            done
            ;;
        3)
            echo "";
            echo "######### "$opt" ##########";
            echo "";
                
            echo -en "Event Log Files Extraction (Windows Vista+) by EVTXExport (\e[96;4mpending...\e[39;24m)"
            output=`fdfind -e evtx . /mnt/temp/ -x bash ./evtxgeneration.sh {} {/.} "$dia"`;
            echo -e "\r\e[KEvent Log Files Extraction (Windows Vista +) by EVTXExport (\e[92;4mDONE!\e[39;24m)"

            echo -en "Event Log Files Extraction (Windows XP/2003) by EVTXExport (\e[96;4mpending...\e[39;24m)"
            output=`fdfind -e evt . /mnt/temp/ -x cp {} ./../results/EventLogs/{/.}."$dia".evt`;
            echo -e "\r\e[KEvent Log Files Extraction (Windows XP/2003) by EVTXExport (\e[92;4mDONE!\e[39;24m)"

            echo -en "Logon Event Log Files Timeline (Windows Vista+) by event2timeline (Python2.7) (\e[96;4mpending...\e[39;24m)"
            output=`fdfind -e evtx . /mnt/temp/ -x bash ./eventtimeline.sh {} {/.} "$dia"`;
            echo -e "\r\e[KLogon Event Log Files Timeline (Windows Vista+) by event2timeline (Python2.7) (\e[92;4mDONE!\e[39;24m)"



            
            
            
            ;;

        
        4)  
            echo "";
            echo "######### "$opt" ##########";
            echo "";
            suboptions=("FLS Timeline (MFT Human-Readable)" "NTFS Parser Timeline (MFT)" "Log2timeline Timeline (All files)" "Quit");
            select opt in "${suboptions[@]}"
            do
                case "$REPLY" in
                    1)  
                        # On récupere les activités des fichiers par la MFT
                        echo -en "Extracting and creating MFT timeline with FLS (\e[96;4mpending...\e[39;24m)"
                        command=`fls -a -h -p -r -m / -o ${offset} ${1} > ./../results/timelines/temp.body`;
                        echo -e "\r\e[KExtracting and creating MFT timeline with FLS (\e[92;4mDONE!\e[39;24m)"

                        # On crée la timeline human-readable de la MFT extraite
                        echo -en "Beautifying MFT timeline with Mactime (\e[96;4mpending...\e[39;24m)"
                        command=`mactime -d -b ./../results/timelines/temp.body > ./../results/timelines/fls."${dia}".csv`;
                        echo -e "\r\e[KBeautifying MFT timeline with Mactime (\e[92;4mDONE!\e[39;24m)"  
                        echo "";                      
                        output=`rm ./../results/timelines/temp.body`;
                        ;;
                    
                    2)  
                        if [[ "$profile" == *"Windows"* ]]
                        then
                            bytesSector=`expr $offset \* 512`
                            path='./../results/timelines/ntfsparser.'${dia}'.csv'

                            echo -en "Extracting and creating MFT timeline with ntfs_parser (\e[96;4mpending...\e[39;24m)"
                            extract=`python3 ./../tools/dfir_ntfs/ntfs_parser --all-mft ${1} "${bytesSector}" ${path}`;
                            echo -e "\r\e[KExtracting and creating MFT timeline with ntfs_parser (\e[92;4mDONE!\e[39;24m)"
                
                            echo "MFT Extracted -> Timeline Created : $path";
                            echo "";
                        else
                            echo "$opt disponible uniquement pour les dump Windows (NTFS)";
                        fi
                        ;;

                    3)  
                        # On crée la timeline générale des fichiers du dump (artéfacts importants et filtrées) par log2timelines
                        echo -en "Timeline Dump Generation by Log2Timeline (\e[96;4mpending...\e[39;24m)"
                        command=`sudo log2timeline.py -f filter_windows.txt -z UTC --volumes all --partitions all --logfile /dev/null -q --no_dependencies_check ./../results/timelines/temp.plaso "$1" > /dev/null 2>&1`;
                        echo -e "\r\e[KTimeline Dump Generation by Log2Timeline (\e[92;4mDONE!\e[39;24m)"  

                        # On transforme la sauvegarde de la timeline en format csv
                        echo -en "Converting Timeline Dump in CSV with Psort (\e[96;4mpending...\e[39;24m)"
                        command=`sudo psort.py --output_time_zone UTC -o l2tcsv ./../results/timelines/temp.plaso --logfile /dev/null -q -w ./../results/timelines/l2t."$dia".csv > /dev/null 2>&1`;
                        echo -e "\r\e[KConverting Timeline Dump in CSV with Psort (\e[92;4mDONE!\e[39;24m)"
                        echo "";                      
                        output=`sudo rm ./../results/timelines/temp.plaso`;
                        ;;
                    
                    4)  
                        echo "";
                        echo "";
                        break
                        ;;
                    *)
                        echo "Choix inconnu";
                        echo "";
                        echo "";
                        ;;
                esac
                REPLY= ;
            done
            ;;
            ################### Encore en cours de dev ###############################

        5)  
            # On démonte le dump s'il y en a un avant de quitter
            if grep -qs '/mnt/temp ' /proc/mounts; 
            then
                extract=`sudo umount /mnt/temp`;
            fi
            echo "";
            echo "######### Arret de l'analyse ##########";
            echo "";
            break;
            ;;
        

        *) 
            echo "Choix inconnu"; 
            echo "";
            echo "";
            ;;
    esac
    # Reset de la variable REPLY pour réafficher le menu apres un choix (a voir si indispensable)
    REPLY= ;

done